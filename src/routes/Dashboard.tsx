import { Switch } from "react-router-dom";
import Customers from "../pages/Dashboard/Customers";
import { Panel } from "../pages/Dashboard/Home";
import Route from "./Route";



const DashboardRoutes: React.FC = () => (
  <Switch>
    <Route exact path={"/dashboard/customers"} component={Customers}/>
    <Route exact path={"/dashboard/"} component={Panel}/>
  </Switch>
);

export default DashboardRoutes;