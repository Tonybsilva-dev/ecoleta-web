import { Divider, Flex, Heading, Link, VStack, Image } from '@chakra-ui/react'
import { useForm } from 'react-hook-form';
import logo from '../assets/logo.svg'
import { Button } from '../components/Form/Button';
import { Input } from '../components/Form/Input';
import { validate } from '../utils/validate';
interface ForgotPasswordCredentials {
  email: string;
}

export default function ForgotPassword() {

  const { register, formState: { errors } } = useForm<ForgotPasswordCredentials>();


  return (
    <Flex
      w="100vw"
      h="100vh"
      align="center"
      justify="center"
    >

      <Flex
        as="form"
        w="100%"
        maxWidth={400}
        padding={8}
        bg="white"
        flexDir="column"
        borderRadius={4}
        shadow="0 0 50px rgba(0, 0, 0, 0.1)"
      >
        <Image src={logo} alt="Logomarca - Uma caixa verde com uma interrogação" />
        <Divider my={6} />
        <Heading size="md" alignSelf={"center"}>
          Recuperar Acesso
        </Heading>

        <Divider my={6} />

        <VStack spacing={4}>
          <Input
            label='EMAIL'
            placeholder='Digite seu email'
            type='email'
            name='email'
            error={errors?.email}
            register={register}
            autoComplete="off"
            options={{
              required: 'É necessário informar um email',
              validate: (value: string) => validate.email(value) || 'E-mail inválido.'
            }}
          />
        </VStack>

        <Button type="submit" mt="8">Entrar</Button>

        <Link
          href="/"
          alignSelf="center"
          textDecor="underline"
          mt={4}
          fontSize="sm"
          color="gray.600"
          _hover={{ color: 'gray.700' }}
          _active={{ color: 'gray.800' }}
        >
          Voltar ao login
        </Link>
      </Flex>
    </Flex>
  )
}