import {
  BrowserRouter as Router
} from "react-router-dom";
import DashboardRoutes from "../../routes/Dashboard";
import Layout from "../../components/Layout";

export default function App() {
  return (
    <Router>
      <Layout>
        <DashboardRoutes />
      </Layout>
    </Router>
  );
}