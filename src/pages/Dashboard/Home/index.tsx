
import { Box, Flex, Grid, GridItem, Heading, HStack, List, Container, ListItem, Modal, ModalBody, ModalCloseButton, ModalContent, ModalFooter, ModalHeader, ModalOverlay, Text, useColorModeValue, useDisclosure, } from '@chakra-ui/react'
import { useEffect, useState } from 'react';
import axios from 'axios';
import { RiAddLine } from 'react-icons/ri'
import { Button } from '../../../components/Form/Button'
import { Input } from '../../../components/Form/Input';
import { useForm } from 'react-hook-form';

import { MapContainer, TileLayer, Marker, Popup } from 'react-leaflet';


interface IBGEUFResponse {
  sigla: string;
}

interface IBGECityResponse {
  nome: string;
}

export const Panel: React.FC = () => {

  const { register, handleSubmit, formState: { errors } } = useForm<any>();

  const OverlayOne = () => (
    <ModalOverlay
      bg='blackAlpha.300'
      backdropFilter='blur(10px) hue-rotate(90deg)'
    />
  )

  const { isOpen, onOpen, onClose } = useDisclosure()
  const [overlay, setOverlay] = useState(<OverlayOne />)

  const [ufs, setUfs] = useState<string[]>([]);
  const [cities, setCities] = useState<string[]>([]);
  const [selectedItems, setSelectedItems] = useState<number[]>([])
  const [initialPosition, setInitialPosition] = useState<[number, number]>([0, 0]);

  const [selectedUf, setSelectedUf] = useState('0');
  const [selectedCity, setSelectedCity] = useState('0');
  const [selectedPosition, setSelectedPosition] = useState<[number, number]>([0, 0]);

  useEffect(() => {
    navigator.geolocation.getCurrentPosition(position => {
      const { latitude, longitude } = position.coords;
      setInitialPosition([latitude, longitude]);
    })
  }, [])

  useEffect(() => {
    axios.get<IBGEUFResponse[]>('https://servicodados.ibge.gov.br/api/v1/localidades/estados?orderBy=nome').then(response => {
      const ufInitials = response.data.map(uf => uf.sigla)
      setUfs(ufInitials);
    });
  }, []);


  useEffect(() => {
    // Carregar as cidades sempre que a UF mudar 
    if (selectedUf === '0') {
      return;
    }
    axios.get<IBGECityResponse[]>(`https://servicodados.ibge.gov.br/api/v1/localidades/estados/${selectedUf}/municipios?orderBy=nome`).then(response => {
      const cityNames = response.data.map(city => city.nome)
      setCities(cityNames);
    });
  }, [selectedUf]);

  return (
    <Box
      width={{ base: '100%', sm: '50%', md: '100%', lg: '100%' }}
      minHeight={'calc(100vh - 12rem)'}
      maxHeight={'calc(100vh - 12rem)'}

    >
      <Flex w={'100%'}>
        <Box
          w={'100%'}
          p="8"
          justifyContent={"space-between"}>
          <Flex mb="8" justifyContent="space-between" alignItems="center" flex={1} >
            <Box>
              <Heading size="lg" fontWeight="medium">Mapa</Heading>
              <Text mt="1" color={useColorModeValue('gray.400', 'gray.400')}>Listagem dos pontos de coleta mais próximos</Text>
            </Box>
            <HStack>
              <Button size="md" leftIcon={<RiAddLine size="24" />} onClick={() => {
                setOverlay(<OverlayOne />)
                onOpen()
              }}>
                Adicionar
              </Button>
            </HStack>
          </Flex>
          <Box overflowY={'auto'}>
            <List
              spacing={3}
              overflowY={'auto'}
              size={'md'}
              display={'absolute'}
              height={{ base: '100%', sm: 'flex', md: 'flex', lg: '55vh' }}
              maxHeight={{ base: '100%', sm: 'flex', md: 'flex', lg: 'flex' }}>
              <ListItem>
                Lorem ipsum dolor sit amet, consectetur adipisicing elit
              </ListItem>
            </List>
          </Box>
        </Box>
      </Flex>

      <Modal isCentered isOpen={isOpen} onClose={onClose} size="full">
        {overlay}
        <ModalContent>
          <ModalHeader>Cadastar ponto de coleta</ModalHeader>
          <ModalCloseButton />
          <ModalBody>
            <Grid
              templateAreas={`"header header"
                  "nav main"
                  "nav footer"`}
              gridTemplateRows={'50px 1fr 100px'}
              gridTemplateColumns={'500px 1fr'}
              h='78vh'
              gap='1'
              color='blackAlpha.700'
              fontWeight='bold'
            >
              <GridItem pl='2' bg='orange.300' area={'header'}>
                Descrição
              </GridItem>
              <GridItem pl='2' bg='pink.300' area={'nav'}>
                Mapa
              </GridItem>
              <GridItem pl='2' bg='green.300' area={'main'}>
                Formulário
              </GridItem>
              <GridItem pl='2' bg='blue.300' area={'footer'}>
                Items
              </GridItem>
            </Grid>
          </ModalBody>
          <ModalFooter gap={2}>
            <Button onClick={onClose} colorScheme="red" >Cancelar</Button>
            <Button onClick={onClose}>Cadastrar</Button>
          </ModalFooter>
        </ModalContent>
      </Modal>
    </Box>
  )
}