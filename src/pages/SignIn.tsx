import { useState } from 'react';
import { Divider, Flex, Link, VStack, Image, InputGroup, InputRightElement, Button as ChakraButton } from '@chakra-ui/react'
import { Button } from '../components/Form/Button'
import { Input } from '../components/Form/Input'
import { SignInCredentials, useAuth } from '../context/AuthContext';
import { useForm, SubmitHandler } from 'react-hook-form';
import { validate } from '../utils/validate';
import { RiEyeFill, RiEyeOffFill} from 'react-icons/ri'

import logo from '../assets/logo.svg'

const SignIn: React.FC = () => {

  const { signIn } = useAuth()
  const { register, handleSubmit, formState: { errors } } = useForm<SignInCredentials>();

  const [show, setShow] = useState(false)
  const handleClick = () => setShow(!show)

  const onSubmit: SubmitHandler<SignInCredentials> = async (data) => {
    await signIn(data);
  }

  return (

    <Flex
      w="100vw"
      h="100vh"
      align="center"
      justify="center"
    >
      

      <Flex
        as="form"
        w="100%"
        maxWidth={400}
        padding={8}
        bg="white"
        flexDir="column"
        borderRadius={4}
        shadow="0 0 20px rgba(0, 0, 0, 0.05)"
        onSubmit={handleSubmit(onSubmit)}
      >
        <Image src={logo} alt="Logomarca - Uma caixa verde com uma interrogação" />
        {/* <Heading size="md" alignSelf={"center"}>
          CMP - Painel Administrativo
        </Heading> */}

        <Divider my={6} />
        <VStack spacing={4}>
          <Input
            placeholder='Digite seu email'
            type='email'
            name='email'
            error={errors?.email}
            register={register}
            autoComplete="off"
            options={{
              required: 'É necessário informar um email',
              validate: (value: string) => validate.email(value) || 'E-mail inválido.'
            }}
          />
          <InputGroup size="md">
            <Input
              placeholder='Digite sua senha'
              type={show ? 'text' : 'password'}
              name='password'
              error={errors?.password}
              register={register}
              autoComplete="off"
              options={{
                required: 'É necessário informar uma senha'
              }}
            />
            <InputRightElement width="auto" mr={1}>
              <ChakraButton backgroundColor={"#E8F0FE"} size="sm" onClick={handleClick}>
                {show ? <RiEyeOffFill color='#333333'/> : <RiEyeFill color='#333333'/>}
              </ChakraButton>
            </InputRightElement>
          </InputGroup>
        </VStack>
        <Button type="submit" mt="8">Entrar</Button>

        <Link
          href="/forgot-password"
          alignSelf="center"
          textDecor="underline"
          mt={4}
          fontSize="sm"
          color="gray.600"
          _hover={{ color: 'gray.700' }}
          _active={{ color: 'gray.800' }}
        >
          Esqueceu sua senha?
        </Link>
      </Flex>
    </Flex>

  )
}

export default SignIn