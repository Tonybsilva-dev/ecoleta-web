import { useToast } from "@chakra-ui/react";
import { createContext, PropsWithChildren, useContext } from "react";

interface ContextProps {
  toast: {
    success: (title: string, description?: string) => void
    error: (title: string, description?: string) => void
    info: (title: string, description?: string) => void
  }
}

const ToastContext = createContext<ContextProps>({} as ContextProps);

export const ToastProvider: React.FC<PropsWithChildren> = ({ children }) => {

  const _toast = useToast();

  const toast = {
    success: (title: string, description = '') => {
      _toast({
        title: title,
        description: description,
        status: 'success',
        duration: 4000,
        isClosable: true,
        position: 'top-right'
      })
    },
    error: (title: string, description = '') => {
      _toast({
        title: title,
        description: description,
        status: 'error',
        duration: 4000,
        isClosable: true,
        position: 'top-right'
      })
    },
    info: (title: string, description = '') => {
      _toast({
        title: title,
        description: description,
        status: 'info',
        duration: 4000,
        isClosable: true,
        position: 'top-right'
      })
    }
  }

  return <ToastContext.Provider value={{ toast }}>
    {children}
  </ToastContext.Provider>
}

export function useAppToast(): ContextProps {
  const context = useContext(ToastContext)

  if (!context) {
    throw new Error('useAppToast must be used within a ToastProvider')
  }

  return context
}
