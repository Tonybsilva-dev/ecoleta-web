import { Box, Flex, useColorModeValue } from '@chakra-ui/react'
import { Header } from './Header'
import Footer from './Footer'

export default function Layout({ children }: any) {
  return (
    <>
      <Header />
      <Flex m={6} gap={6} flex={1}>
        <Box
          flex="1"
          bgColor={useColorModeValue('white', '#182747')}
          shadow="0 0 20px rgba(0, 0, 0, 0.05)"
          borderRadius={4}>
          <Flex
            // maxH={'calc(100vh-20)'}
            bgColor={useColorModeValue('white', '#182747')}>
            {children}
          </Flex>
        </Box>
      </Flex>
      <Footer />
    </>
  )
}
