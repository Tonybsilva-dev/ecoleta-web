import { VStack, Link, Text, Flex, useColorModeValue } from "@chakra-ui/react";
import { RiDashboardLine, RiContactsLine, RiPencilRulerLine, RiInputMethodLine } from "react-icons/ri";

export function Sidebar() {

  const geralRoutes = [
    { name: 'Painel', path: '/dashboard', icon: <RiDashboardLine size={20} /> },
    { name: 'Clientes', path: '/dashboard/customers', icon: <RiContactsLine size={20} /> },
    { name: 'Produtos', path: '/dashboard/products', icon: <RiPencilRulerLine size={20} /> },
  ]

  const registerRoutes = [
    { name: 'Relatórios', path: '/dashboard/reports', icon: <RiInputMethodLine size={20} /> },
  ]

  const currentRoute = window.location.pathname
  const h1TextColor = useColorModeValue('green.500', '#E0C097')
  const defaultTextColor = useColorModeValue('green.500', 'white')

  return (
    <Flex
      as="aside"
      w="72"
      bgColor={useColorModeValue('white', '#182747')}
      py="8"
      mx="6"
      shadow="0 0 20px rgba(0, 0, 0, 0.05)"
      borderRadius={4}
      direction="column"
    >
      <VStack spacing="4" pr="8" mt={8} alignItems="stretch">
        <Text fontWeight="bold" color={useColorModeValue('green.500', '#E0C097')} fontSize="small" px={8}>GERAL</Text>
        {geralRoutes.map((route, index) => {
          const isOpen = currentRoute === route.path;
          return (
            <Link display="flex" alignItems="center" color={isOpen ? defaultTextColor : "gray.500"} py="1" pl={8} borderLeft={isOpen ? "5px solid" : ""} href={route.path} key={'link-' + index}>
              {route.icon}
              <Text ml="4" fontSize="medium" fontWeight="medium" color={isOpen ? defaultTextColor : "gray.500"}> {route.name} </Text>
            </Link>
          )
        })}
      </VStack>
      <VStack spacing="4" pr="8" mt={8} alignItems="stretch">
        <Text fontWeight="bold" color={useColorModeValue('green.500', '#E0C097')} fontSize="small" px={8}>REGISTROS</Text>
        {registerRoutes.map((route, index) => {
          const isOpen = currentRoute === route.path;
          console.log(isOpen)
          return (
            <Link display="flex" alignItems="center" color={isOpen ? "green.500" : "gray.500"} py="1" pl={8} borderLeft={isOpen ? "3px solid" : ""} href={route.path} key={'link-' + index}>
              {route.icon}
              <Text ml="4" fontSize="medium" fontWeight="medium"> {route.name} </Text>
            </Link>
          )
        })}
      </VStack>
    </Flex >
  );
}