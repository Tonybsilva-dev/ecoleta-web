import { Flex, Avatar, AvatarBadge, Box, Text, Image, Spacer, ButtonGroup, Menu, MenuButton, IconButton, MenuList, MenuItem, MenuDivider, MenuGroup, useColorMode, useColorModeValue } from "@chakra-ui/react";
// import { useAuth } from "../../../context/AuthContext";
import { RiArrowDownSLine, RiLogoutBoxLine, RiSettings2Line, RiBookletLine, RiUser3Line, RiInformationLine, RiTShirtLine } from 'react-icons/ri'
import logo from '../../../assets/logo.svg'
import logoDark from '../../../assets/logo-dark.svg'

export function Header() {
  const { colorMode, toggleColorMode } = useColorMode()
  // const { user, signOut } = useAuth()
  // const { name, email, avatar } = user as any

  return (
    <Flex
      as="header"
      h="20"
      bgColor={useColorModeValue('white', '#182747')}
      px="8"
      shadow="0 0 20px rgba(0, 0, 0, 0.05)"
      alignItems="center">
      <Box p='2'>
        <Image src={useColorModeValue(logo, logoDark)} />
      </Box>
      <Spacer />
      <ButtonGroup gap='2' alignItems={"center"} justifyContent={"center"}>
        <Box>
          <Text fontWeight="medium" autoCapitalize="on" style={{ textTransform: 'uppercase' }}>Antonio S</Text>
          <Text color={useColorModeValue('gray.800', 'white')} fontSize="small"> tonybsilvadev@gmail.com </Text>
        </Box>
        <Avatar size="md" name={"Avatar"} src={""}>
          <AvatarBadge borderColor="papayawhip" bg="green.500" boxSize="1.25rem" />
        </Avatar>
        <Menu>
          <MenuButton
            as={IconButton}
            aria-label='Options'
            icon={<RiArrowDownSLine />}
            variant='outline'
            size="md"
          />
          <MenuList>
            <MenuGroup title='Perfil'>
              <MenuItem icon={<RiUser3Line size={20} />} command=''>
                Minha conta
              </MenuItem>
            </MenuGroup>
            <MenuDivider />
            <MenuGroup title='Ajuda'>
              <MenuItem icon={<RiBookletLine size={20} />} command=''>
                Notas
              </MenuItem>
              <MenuItem icon={<RiInformationLine size={20} />} command=''>
                Sobre
              </MenuItem>
            </MenuGroup>
            <MenuDivider />
            <MenuGroup title='Sistema'>
              <MenuItem icon={<RiSettings2Line size={20} />} command='' disabled>
                Configurações
              </MenuItem>
              <MenuItem icon={<RiTShirtLine size={20} />} command='' onClick={toggleColorMode}>
                Tema: {colorMode === 'light' ? 'Dark' : 'Light'}
              </MenuItem>
              <MenuItem icon={<RiLogoutBoxLine size={20} />} command='⌘ESC'>
                Sair
              </MenuItem>
            </MenuGroup>
          </MenuList>
        </Menu>
      </ButtonGroup>
      {/* <Flex alignItems="center" w="60" mr="8">
        <Avatar size="md" name={name} src={avatar}>
          <AvatarBadge borderColor="papayawhip" bg="green.500" boxSize="1.25rem" />
        </Avatar>
        <Box ml="4">
          <Text fontWeight="medium" autoCapitalize="on" style={{ textTransform: 'uppercase' }}>{name}</Text>
          <Text color="gray.500" fontSize="small">{email}</Text>
        </Box>
      </Flex> */}
    </Flex>
  );
}